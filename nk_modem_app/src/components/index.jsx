import Navbar from "./Navbar"
import Hero from "./Hero";
import Stats from "./Stats";
import Business from "./Business";
import CardDeal from "./CardDeal";
import Clients from "./Clients";
import CTA from "./CTA"
import Testimonials from "./Testimonials";
import Footer from "./Footer";
import Button from "./Button";
import GetStarded from "./GetStarded"
import FeedBackCard from "./FeedBackCard";
import Billing from "./Billing";

export {
   Navbar,
    Hero,
    Stats,
    Billing,
    Business,
    CardDeal,
    Clients,
    CTA,
    Testimonials,
    Footer,
    Button,
    GetStarded,
    FeedBackCard,
}

