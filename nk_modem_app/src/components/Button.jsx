

function Button(styles) {
  return (
      <button type="button" className={`py-4 px-6 bg-blue-gradient text-primary text-[18px] outline-none
      ${styles} font-poppins font-medium mt-10 rounded-[10px]`}>
         Get Started
      </button>
  )
}

export default Button
